package com.utn.TP4.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Getter
@Setter

public class Log {
    private String so;
    private String browser;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    protected Log(){ }

    public Log(String so, String browser) {
        this.so = so;
        this.browser = browser;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d, so='%s', browser='%s']",
                id, so, browser);
    }

}
