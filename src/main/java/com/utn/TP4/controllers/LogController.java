package com.utn.TP4.controllers;
import com.utn.TP4.models.Log;
import com.utn.TP4.services.LogService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("")
public class LogController {
    @Autowired
    LogService logService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Log> index() {
        return logService.getAll();
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Log index(@PathVariable("id") long id) {
        return logService.getLogByID(id);
    }
    @RequestMapping(method = RequestMethod.POST)
    public Log store(@RequestBody Log log) {
        return logService.save(log);
    }

}
