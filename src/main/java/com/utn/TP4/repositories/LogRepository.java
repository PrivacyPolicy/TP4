package com.utn.TP4.repositories;
import com.sun.xml.internal.bind.v2.model.core.ID;
import com.utn.TP4.models.Log;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepository extends JpaRepository<Log, Long> {

    Log findById(long id);
    List<Log> findAll();
}