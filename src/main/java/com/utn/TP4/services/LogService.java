package com.utn.TP4.services;
import com.utn.TP4.models.Log;
import com.utn.TP4.repositories.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogService {
    @Autowired
    LogRepository logRepository;


    public List<Log> getAll() {
        return logRepository.findAll();
    }
    public Log getLogByID(long id) {
        return logRepository.findById(id);
    }
    public Log save(Log log) {
        return logRepository.save(log);
    }
}
