package com.utn.TP4;
import com.utn.TP4.models.Log;
import com.utn.TP4.repositories.LogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;

import com.utn.TP4.controllers.LogController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class Tp4Application {

    private static final Logger log = LoggerFactory.getLogger(Tp4Application.class);


    public static void main(String[] args) {

        SpringApplication.run(Tp4Application.class, args);
    }

    @Bean
    public CommandLineRunner setup(LogRepository repository) {
        return (args) -> {
            // save a couple of customers
            repository.save(new Log("OSX", "Opera"));
            repository.save(new Log("Windows 7", "Mozilla"));
            repository.save(new Log("Windows 10", "Chrome"));
            repository.save(new Log("Windows 8", "IE"));
            repository.save(new Log("Android", "Chrome"));


            // fetch all customers
            log.info("Devices found with findAll():");
            log.info("-------------------------------");
            for (Log data : repository.findAll()) {
                log.info(data.toString());
            }
            log.info("");


        };
    }
}
