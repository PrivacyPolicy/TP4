TP4

Maven

Goals
-mvn clean: Clean se utiliza cuando quieres borrar archivos generados en tiempo de ejecucuin en el directorio de un proyecto.
-mvn compile: Compile se utiliza para compilar el codigo fuente del proyecto
-mvn package: Con Package se toma el codigo compilado y se los empaqueta en su formato distrubuible, como por ejemplo JAR.
-mvn install: Se instala el package en el repositorio local para usarlo como dependencia en otros proyectos localmente.

Scopes
compile: Es el scope por defecto utilizado si no se especifica por ningun otro. Las dependencias de compilacion estan disponibles en todos los classpaths de un proyecto.
provided: Es parecido a compile, pero espera que el JDK o un contenedor provea la dependencia en tiempo de ejecucion.
runtime: Indica que esta dependencia no es necesaria para compilacion pero si para ejecucion.
test: Indica que esta dependencia no es necesaria para el uso normal de la aplicacion. Este scope no es transitivo.
system: Es similar a provided solo que tenes que proveer el JAR que la contiene explicitamente. El artefacto esta siempre disponible y no lo buscara en un repositorio.
import: Este scope solo es suporteado en una dependencia de tipo Pom en la seccion de <dependencyManagement>. Indica que dependencia debe remplazarse con la lista de dependencias en el Pom especificado.

¿Que es un Archetype?
Un Archetype es una herramienta para hacer templates en Maven. Un Archetype es definido como un patron o modelo original, de donde todas las otras cosas del mismo tipo son hechas.

Defina la estructura de un proyecto de Maven
src/main/java: Aqui estara la estructura de las clases de nuestro proyecto. Nuestras controladoras, modelos, servicios y repositorios.
src/main/resources: Aqui guardaremos archivos de propiedades o configuracion y recursos como imagenes.
src/test/java: Aqui tendremos las clases encargadas de probar el proyecto

Defina la diferencia entre un Archetype y un Artifact
Un Archetype es un template que determinara la estructura del proyecto. Un Artifact es un archivo (generalmente JAR) que se implementa en un repositorio Maven

Spring

Explique los 4 stereotypes basicos
@Component: Es el estereotipo principal, indica que la clase anotada es un component (o un Bean de Spring).
@Repository, @Service y @Controller son especializaciones de @Component para casos concretos (persistencia, servicios y presentación). Esto significa que puede usarse siempre @Component pero lo adecuado es usar estos estereotipos ya que algunas herramientas o futuras versiones de Spring pueden añadir semántica adicional.
@Autowired sirve para inyectar un Bean usando la autodetección de Spring.

REST
Significa REpresentational State Transfer, es un tipo de arquitectura de desarrollo web que se apoya totalmente en el estándar HTTP.

Para manipular los recursos, HTTP nos dota de los siguientes métodos con los cuales debemos operar:

GET: Para consultar y leer recursos
POST: Para crear recursos
PUT: Para editar recursos
DELETE: Para eliminar recursos.
PATCH: Para editar partes concretas de un recurso.


